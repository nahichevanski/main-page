package server

import (
	"context"
	budgetpage "main-page/internal/server/handlers/budget/page"
	"main-page/internal/server/handlers/home"
	"net/http"

	"main-page/internal/config"
	servicebudget "main-page/internal/miniapp/service/budget"
	serviceauth "main-page/internal/miniapp/service/sso/auth"
	"main-page/internal/server/handlers/budget/add"
	"main-page/internal/server/handlers/budget/delete"
	"main-page/internal/server/handlers/budget/get"
	authpage "main-page/internal/server/handlers/sso/auth/page"
	"main-page/internal/server/handlers/sso/auth/sign"
	"main-page/internal/server/middleware"

	"github.com/gin-gonic/gin"
)

type Server struct {
	server        *http.Server
	serviceAuth   serviceauth.Auth
	serviceBudget servicebudget.Budget
}

func New(cfg *config.Config, auth serviceauth.Auth, budget servicebudget.Budget) *Server {
	server := &http.Server{
		Addr:         cfg.Server.Addr,
		ReadTimeout:  cfg.Server.Timeout,
		WriteTimeout: cfg.Server.Timeout,
		IdleTimeout:  cfg.Server.IdleTimeout,
		Handler:      initRoutes(cfg, auth, budget),
	}

	return &Server{
		server:        server,
		serviceAuth:   auth,
		serviceBudget: budget,
	}
}

func (s *Server) Run() error {
	//gin.SetMode(gin.ReleaseMode)
	return s.server.ListenAndServe()
}

func (s *Server) Stop(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}

func initRoutes(cfg *config.Config, auth serviceauth.Auth, budget servicebudget.Budget) http.Handler {
	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	r.Static("/css", "./css")
	r.LoadHTMLGlob("html/*")

	r.GET("/", home.Home())
	r.GET("/auth/sign-in", authpage.SignIn())
	r.GET("/auth/sign-up", authpage.SignUp())
	r.POST("/auth/sign-up", sign.Up(auth, cfg))
	r.POST("/auth/sign-in", sign.In(auth, cfg))
	protected := r.Group("/", middleware.Auth(auth, cfg))
	{
		protected.GET("/budget", home.Budget())
		protected.GET("budget/add-cost", budgetpage.AddCost())
		protected.GET("budget/add-income", budgetpage.AddIncome())
		protected.POST("budget/cost", add.Cost(budget))
		protected.POST("budget/income", add.Income(budget))
		protected.GET("budget/delete-cost", budgetpage.DeleteCost())
		protected.GET("budget/delete-income", budgetpage.DeleteIncome())
		protected.DELETE("budget/cost/:id", delete.Cost(budget))
		protected.DELETE("budget/income/:id", delete.Income(budget))
		protected.GET("budget/cost-by-date", budgetpage.GetListCostByDate())
		protected.GET("budget/income-by-date", budgetpage.GetListIncomeByDate())
		protected.GET("budget/cost-by-date/:date", get.ListCostByDate(budget))
		protected.GET("budget/income-by-date/:date", get.ListIncomeByDate(budget))
		protected.GET("budget/cost-by-month", budgetpage.GetMonthListCost())
		protected.GET("budget/cost-by-year", budgetpage.GetYearListCost())
		protected.GET("budget/cost-by-week", get.WeekListCost(budget))
		protected.GET("budget/cost-by-month/:month", get.MonthListCost(budget))
		protected.GET("budget/cost-by-year/:year", get.YearListCost(budget))
		protected.GET("budget/income-by-month", budgetpage.GetMonthListIncome())
		protected.GET("budget/income-by-year", budgetpage.GetYearListIncome())
		protected.GET("budget/income-by-month/:month", get.MonthListIncome(budget))
		protected.GET("budget/income-by-year/:year", get.YearListIncome(budget))
		protected.GET("/auth/sign-out", sign.Out(auth))
		protected.GET("/auth/maybe-delete", authpage.MaybeDelete())
		protected.DELETE("/auth/delete", sign.Delete(auth))
	}

	return r
}
