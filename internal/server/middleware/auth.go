package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"main-page/internal/config"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/sso/auth"
	"net/http"
	"os"
	"time"
)

func Auth(service auth.Auth, cfg *config.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		aToken, err := c.Cookie("AccessToken")
		if err != nil {
			html.ErrorWithAbort(c, http.StatusUnauthorized, err.Error())
			return
		}

		rToken, err := c.Cookie("RefreshToken")
		if err != nil {
			html.ErrorWithAbort(c, http.StatusUnauthorized, err.Error())
			return
		}

		accessTokenCookie, err := jwt.Parse(aToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, jwt.ErrSignatureInvalid
			}
			return []byte(os.Getenv("SECRET_KEY")), nil
		})
		if err != nil {
			html.ErrorWithAbort(c, http.StatusUnauthorized, err.Error())
			return
		}

		if claims, ok := accessTokenCookie.Claims.(jwt.MapClaims); ok && accessTokenCookie.Valid {
			userID := claims["uid"].(float64)

			if claims["custom_exp"].(float64) < float64(time.Now().Unix()) {

				token, err := service.Refresh(c.Request.Context(), userID, aToken, rToken)
				if err != nil {
					html.ErrorWithAbort(c, http.StatusUnauthorized, err.Error())
					return
				}

				c.SetSameSite(http.SameSiteLaxMode)
				c.SetCookie("AccessToken", token.AccessToken, cfg.Cookie.MaxAge, "", "", false, true)
				c.SetCookie("RefreshToken", token.RefreshToken, cfg.Cookie.MaxAge, "", "", false, true)

				c.Set("userID", userID)
				c.Next()

			}

			c.Set("userID", userID)

			c.Next()

		} else {
			html.ErrorWithAbort(c, http.StatusInternalServerError, err.Error())
			return
		}
	}
}
