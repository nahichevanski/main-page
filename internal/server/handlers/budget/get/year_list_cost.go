package get

import (
	"github.com/gin-gonic/gin"
	d "main-page/internal/lib/date"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/budget"
	"net/http"
)

func YearListCost(service budget.Budget) gin.HandlerFunc {
	return func(c *gin.Context) {
		userIdForm, ok := c.Get("userID")
		if !ok {
			html.Error(c, http.StatusUnauthorized, "Нет userID в контексте запроса")
			return
		}

		userID, ok := userIdForm.(float64)
		if !ok {
			html.Error(c, http.StatusBadRequest, "Неверный тип userID")
			return
		}

		yearParam := c.Param("year")
		if yearParam == "" {
			html.Error(c, http.StatusBadRequest, "Нет года в url")
			return
		}

		yearInt, yearStr := d.Year(yearParam)

		list, err := service.YearListCostFromBudget(c.Request.Context(), int64(userID), yearInt)
		if err != nil {
			html.DecodeError(c, err)
			return
		}

		c.HTML(http.StatusOK, "sum_list.html", gin.H{
			"result":  "Траты за " + yearStr,
			"message": list,
		})
	}
}
