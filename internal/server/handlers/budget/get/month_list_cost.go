package get

import (
	"github.com/gin-gonic/gin"
	d "main-page/internal/lib/date"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/budget"
	"net/http"
	"strconv"
)

func MonthListCost(service budget.Budget) gin.HandlerFunc {
	return func(c *gin.Context) {
		userIdForm, ok := c.Get("userID")
		if !ok {
			html.Error(c, http.StatusUnauthorized, "Нет userID в контексте запроса")
			return
		}

		userID, ok := userIdForm.(float64)
		if !ok {
			html.Error(c, http.StatusBadRequest, "Неверный тип userID")
			return
		}

		monthParam := c.Param("month")
		if monthParam == "" {
			html.Error(c, http.StatusBadRequest, "Нет номера месяца в url")
			return
		}

		month, err := strconv.Atoi(monthParam)
		if err != nil {
			month = 0
		}
		//TODO везде поменять обработку ошибок на html.DecodeError
		list, err := service.MonthListCostFromBudget(c.Request.Context(), int64(userID), month)
		if err != nil {
			html.DecodeError(c, err)
			return
		}

		c.HTML(http.StatusOK, "sum_list.html", gin.H{
			"result":  "Траты за " + d.MonthName(month),
			"message": list,
		})
	}
}
