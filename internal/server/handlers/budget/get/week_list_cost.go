package get

import (
	"github.com/gin-gonic/gin"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/budget"
	"net/http"
)

func WeekListCost(service budget.Budget) gin.HandlerFunc {
	return func(c *gin.Context) {
		userIdForm, ok := c.Get("userID")
		if !ok {
			html.Error(c, http.StatusUnauthorized, "Нет userID в контексте запроса")
			return
		}

		userID, ok := userIdForm.(float64)
		if !ok {
			html.Error(c, http.StatusBadRequest, "Неверный тип userID")
			return
		}

		list, err := service.WeekListCostFromBudget(c.Request.Context(), int64(userID))
		if err != nil {
			html.Error(c, http.StatusInternalServerError, err.Error())
			return
		}

		c.HTML(http.StatusOK, "sum_list.html", gin.H{
			"result":  "Траты за неделю",
			"message": list,
		})
	}
}
