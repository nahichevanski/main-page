package get

import (
	"github.com/gin-gonic/gin"
	d "main-page/internal/lib/date"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/budget"
	"net/http"
)

func ListIncomeByDate(service budget.Budget) gin.HandlerFunc {
	return func(c *gin.Context) {
		userIdForm, ok := c.Get("userID")
		if !ok {
			html.Error(c, http.StatusUnauthorized, "Нет userID в контексте запроса")
			return
		}

		userID, ok := userIdForm.(float64)
		if !ok {
			html.Error(c, http.StatusBadRequest, "Неверный тип userID")
			return
		}

		date := c.Param("date")
		if date == "" {
			html.Error(c, http.StatusBadRequest, "Нет даты в url")
			return
		}

		list, err := service.ListIncomeByDateFromBudget(c.Request.Context(), int64(userID), date)
		if err != nil {
			html.Error(c, http.StatusInternalServerError, err.Error())
			return
		}

		c.HTML(http.StatusOK, "list_by_date.html", gin.H{
			"result":  "Доходы за " + d.FormatDate(date),
			"message": list,
		})
	}
}
