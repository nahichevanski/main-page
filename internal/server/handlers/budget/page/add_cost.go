package page

import "github.com/gin-gonic/gin"

func AddCost() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "add_cost.html", nil)
	}
}
