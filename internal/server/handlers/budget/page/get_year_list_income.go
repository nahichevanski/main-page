package page

import "github.com/gin-gonic/gin"

func GetYearListIncome() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "year_list_income.html", nil)
	}
}
