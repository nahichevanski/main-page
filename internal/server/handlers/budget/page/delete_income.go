package page

import "github.com/gin-gonic/gin"

func DeleteIncome() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "delete_income.html", nil)
	}
}
