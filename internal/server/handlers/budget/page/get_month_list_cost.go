package page

import "github.com/gin-gonic/gin"

func GetMonthListCost() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "month_list_cost.html", nil)
	}
}
