package page

import "github.com/gin-gonic/gin"

func GetListCostByDate() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "list_cost_by_date.html", nil)
	}
}
