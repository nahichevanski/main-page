package page

import "github.com/gin-gonic/gin"

func GetYearListCost() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "year_list_cost.html", nil)
	}
}
