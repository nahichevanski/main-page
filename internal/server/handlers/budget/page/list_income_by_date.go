package page

import "github.com/gin-gonic/gin"

func GetListIncomeByDate() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "list_income_by_date.html", nil)
	}
}
