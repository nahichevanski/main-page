package page

import "github.com/gin-gonic/gin"

func AddIncome() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "add_income.html", nil)
	}
}
