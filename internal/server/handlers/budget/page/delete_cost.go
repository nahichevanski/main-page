package page

import "github.com/gin-gonic/gin"

func DeleteCost() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "delete_cost.html", nil)
	}
}
