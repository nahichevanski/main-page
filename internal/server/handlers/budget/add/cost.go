package add

import (
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/budget"
	"net/http"
	"strconv"
)

func Cost(service budget.Budget) gin.HandlerFunc {
	return func(c *gin.Context) {
		userIdForm, ok := c.Get("userID")
		if !ok {
			html.Error(c, http.StatusUnauthorized, "Нет userID в контексте запроса")
			return
		}

		userID, ok := userIdForm.(float64)
		if !ok {
			html.Error(c, http.StatusBadRequest, "Неверный тип userID")
			return
		}

		amountForm := c.PostForm("amount")
		err := validation.Validate(amountForm, validation.Required, is.Float)
		if err != nil {
			html.Error(c, http.StatusBadRequest, err.Error())
			return
		}

		amount, err := strconv.ParseFloat(amountForm, 10)
		if err != nil {
			html.Error(c, http.StatusBadRequest, err.Error())
			return
		}

		category := c.PostForm("category_name")
		err = validation.Validate(category, validation.Required)
		if err != nil {
			html.Error(c, http.StatusBadRequest, err.Error())
			return
		}

		comment := c.PostForm("comment")

		costID, err := service.AddCostToBudget(c.Request.Context(), int64(userID), amount, category, comment)
		if err != nil {
			html.Error(c, http.StatusInternalServerError, err.Error())
			return
		}
		c.HTML(http.StatusOK, "ok.html", gin.H{
			"result":  costID,
			"message": "Всё ОК! Этот номер может понадобиться если нужно будет удалить эту запись",
		})
	}
}
