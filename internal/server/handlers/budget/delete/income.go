package delete

import (
	"github.com/gin-gonic/gin"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/budget"
	"net/http"
	"strconv"
)

func Income(service budget.Budget) gin.HandlerFunc {
	return func(c *gin.Context) {
		userIdForm, ok := c.Get("userID")
		if !ok {
			html.Error(c, http.StatusUnauthorized, "Нет userID в контексте запроса")
			return
		}

		userID, ok := userIdForm.(float64)
		if !ok {
			html.Error(c, http.StatusBadRequest, "Неверный тип userID")
			return
		}

		idParam := c.Param("id")
		if idParam == "" {
			html.Error(c, http.StatusBadRequest, "Нет ID дохода в url")
			return
		}

		id, err := strconv.ParseInt(idParam, 10, 64)
		if err != nil {
			html.Error(c, http.StatusBadRequest, "Неверный формат ID дохода в url")
			return
		}

		isDeleted, err := service.DeleteIncomeFromBudget(c.Request.Context(), int64(userID), id)
		if err != nil {
			html.Error(c, http.StatusInternalServerError, err.Error())
			return
		}

		c.HTML(http.StatusOK, "ok.html", gin.H{
			"result":  isDeleted,
			"message": "Запись о  доходе удалена",
		})
	}
}
