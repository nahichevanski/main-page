package home

import (
	"github.com/gin-gonic/gin"
)

func Home() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "home.html", gin.H{})
	}
}

func Budget() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "budget_main_page.html", gin.H{})
	}
}
