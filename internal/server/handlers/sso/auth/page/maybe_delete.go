package page

import "github.com/gin-gonic/gin"

func MaybeDelete() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "delete_user.html", nil)
	}
}
