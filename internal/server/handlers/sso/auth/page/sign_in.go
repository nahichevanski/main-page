package page

import (
	"github.com/gin-gonic/gin"
)

func SignIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "sign_in.html", nil)
	}
}
