package page

import (
	"github.com/gin-gonic/gin"
)

func SignUp() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(200, "sign_up.html", nil)
	}
}
