package sign

import (
	"fmt"
	"google.golang.org/grpc/status"
	"main-page/internal/config"
	"main-page/internal/dto"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/sso/auth"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

func Up(service auth.Auth, cfg *config.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		email := c.PostForm("email")
		err := validation.Validate(email, validation.Required, is.Email)
		if err != nil {
			html.Error(c, http.StatusBadRequest, err.Error())
			return
		}

		password := c.PostForm("password")
		err = validation.Validate(
			password, validation.Required, validation.Length(8, 20), is.Alphanumeric,
		)
		if err != nil {
			html.Error(c, http.StatusBadRequest, err.Error())
			return
		}

		id, err := service.RegisterNewUser(c.Request.Context(), email, password)
		if err != nil {
			html.Error(c, http.StatusBadRequest, err.Error())
			return
		}

		var token dto.Token

		if err == nil && id != 0 {
			token, err = service.EnterUser(c.Request.Context(), email, password)
			if err != nil {
				if st, ok := status.FromError(err); ok {
					if st.Code() == 3 {
						html.Error(c, http.StatusBadRequest, err.Error())
						return
					}
				}
				html.DecodeError(c, err)
				return
			}

			c.SetSameSite(http.SameSiteLaxMode)
			c.SetCookie("AccessToken", token.AccessToken, cfg.Cookie.MaxAge, "", "", false, true)
			c.SetCookie("RefreshToken", token.RefreshToken, cfg.Cookie.MaxAge, "", "", false, true)

		}

		c.HTML(http.StatusOK, "ok.html", gin.H{
			"result":  fmt.Sprintf("Ваш ID: %d", id),
			"message": token,
		})
	}
}
