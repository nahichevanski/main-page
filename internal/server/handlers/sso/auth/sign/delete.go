package sign

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"main-page/internal/lib/html"
	"main-page/internal/miniapp/service/sso/auth"
	"net/http"
)

func Delete(service auth.Auth) gin.HandlerFunc {
	return func(c *gin.Context) {
		userId, exist := c.Get("userID")
		if !exist {
			html.Error(c, http.StatusBadRequest, "userID not exist in gin context")
			return
		}

		uid, ok := userId.(float64)
		if !ok {
			html.Error(c, http.StatusInternalServerError, "userID not convert to int64 from float64")
		}
		_, err := service.DeleteUser(c.Request.Context(), int64(uid))
		if err != nil {
			html.DecodeError(c, err)
			return
		}

		c.SetCookie("AccessToken", "", 0, "", "", false, true)
		c.SetCookie("RefreshToken", "", 0, "", "", false, true)

		c.HTML(http.StatusOK, "ok.html", gin.H{
			"result":  fmt.Sprintf("User with id %d is deleted", int64(uid)),
			"message": "Всё ОК",
		})
	}
}
