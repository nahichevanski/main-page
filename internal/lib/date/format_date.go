package date

import "time"

func FormatDate(inputDate string) string {
	// Парсим входную дату в формате "2006-01-02"
	parsedDate, err := time.Parse("2006-01-02", inputDate)
	if err != nil {
		return inputDate
	}

	// Форматируем дату в нужный нам формат "02.01.2006"
	formattedDate := parsedDate.Format("02.01.2006")

	return formattedDate
}
