package date

import (
	"strconv"
	"time"
)

func Year(year string) (int, string) {
	y, err := strconv.Atoi(year)
	if err != nil || y == 0 {
		yy := time.Now().Year()
		return yy, strconv.Itoa(yy)
	}

	return y, year
}
