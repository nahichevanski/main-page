package date

var monthNames = []string{"", "январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"}

func MonthName(month int) string {
	if month < 1 || month > 12 {
		return "текущий месяц"
	}
	return monthNames[month]
}
