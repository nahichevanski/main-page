package html

import (
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net/http"
)

func Error(c *gin.Context, status int, msg string) {
	c.HTML(status, "error.html", gin.H{
		"head":  "Ошибка",
		"error": msg,
	})
}

func ErrorWithAbort(c *gin.Context, status int, msg string) {
	c.HTML(status, "error.html", gin.H{
		"head":  "Ошибка",
		"error": msg,
	})
	c.Abort()
}

func DecodeError(c *gin.Context, err error) {
	if grpcErr, ok := status.FromError(err); ok {
		switch grpcErr.Code() {
		case codes.NotFound:
			Error(c, http.StatusNotFound, err.Error())
			return
		case codes.InvalidArgument:
			Error(c, http.StatusBadRequest, err.Error())
			return
		case codes.Unauthenticated:
			Error(c, http.StatusUnauthorized, err.Error())
			return
		default:
			Error(c, http.StatusInternalServerError, err.Error())
			return
		}
	}
}
