package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) ListIncomeByDate(ctx context.Context, userID int64, date string) ([]dto.IncomeView, error) {
	const oper = "client.budget.ListIncomeByDate"

	resp, err := b.clientBudget.ListIncomesByDate(ctx, &budget.ListIncomesByDateRequest{
		Date:   date,
		UserId: userID,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	incomes := make([]dto.IncomeView, len(resp.ListIncomes))
	for i, income := range resp.ListIncomes {
		incomes[i] = dto.IncomeView{}
		incomes[i].ID = income.Id
		incomes[i].Amount = income.Amount
		incomes[i].Category = income.Category
		incomes[i].Comment = income.Comment
	}

	return incomes, nil
}
