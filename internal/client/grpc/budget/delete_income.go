package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) DeleteIncome(ctx context.Context, userID, incomeID int64) (bool, error) {
	const oper = "client.budget.DeleteIncome"

	resp, err := b.clientBudget.DeleteIncome(ctx, &budget.DeleteIncomeRequest{
		UserId:   userID,
		IncomeId: incomeID,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return resp.IsDeleted, nil
}
