package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/protobuf/types/known/timestamppb"
	"main-page/internal/lib/sl"
	"time"
)

func (b *ClientBudget) AddIncome(ctx context.Context, userID int64, amount float64, category, comment string) (int64, error) {
	const oper = "client.budget.AddIncome"

	var timeNow = timestamppb.Timestamp{
		Seconds: time.Now().Unix(),
		Nanos:   0,
	}

	resp, err := b.clientBudget.AddIncome(ctx, &budget.AddIncomeRequest{
		UserId:    userID,
		Amount:    amount,
		Category:  category,
		CreatedAt: &timeNow,
		Comment:   comment,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return resp.IncomeId, nil
}
