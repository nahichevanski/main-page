package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) DeleteCost(ctx context.Context, userID, costID int64) (bool, error) {
	const oper = "client.budget.DeleteCost"

	resp, err := b.clientBudget.DeleteCost(ctx, &budget.DeleteCostRequest{
		UserId: userID,
		CostId: costID,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return resp.IsDeleted, nil
}
