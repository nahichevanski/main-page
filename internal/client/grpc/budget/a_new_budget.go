package budget

import (
	"context"
	"fmt"
	grpclog "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/logging"
	grpcretry "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/retry"
	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"log/slog"
	"main-page/internal/config"
)

type ClientBudget struct {
	clientBudget proto.BudgetClient
	log          *slog.Logger
}

func New(ctx context.Context, cfg *config.Config, log *slog.Logger) (*ClientBudget, error) {
	const oper = "client.sso.New"

	retryOpts := []grpcretry.CallOption{
		grpcretry.WithCodes(codes.DeadlineExceeded, codes.NotFound, codes.Aborted),
		grpcretry.WithMax(uint(cfg.BudgetClient.RetriesCount)),
		grpcretry.WithPerRetryTimeout(cfg.BudgetClient.Timeout),
	}

	logOpts := []grpclog.Option{
		grpclog.WithLogOnEvents(grpclog.PayloadReceived, grpclog.PayloadSent),
	}

	conn, err := grpc.DialContext(ctx, cfg.BudgetClient.Addr,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			grpclog.UnaryClientInterceptor(interceptorLogger(log), logOpts...),
			grpcretry.UnaryClientInterceptor(retryOpts...),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return &ClientBudget{
		clientBudget: proto.NewBudgetClient(conn),
		log:          log,
	}, nil
}

// InterceptorLogger adapts slog logger to interceptor logger.
// This code is simple enough to be copied and not imported.
func interceptorLogger(l *slog.Logger) grpclog.Logger {
	return grpclog.LoggerFunc(func(ctx context.Context, lvl grpclog.Level, msg string, fields ...any) {
		l.Log(ctx, slog.Level(lvl), msg, fields...)
	})
}
