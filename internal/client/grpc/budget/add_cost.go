package budget

import (
	"context"
	"fmt"
	"time"

	"main-page/internal/lib/sl"

	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (b *ClientBudget) AddCost(ctx context.Context, userID int64, amount float64, category, comment string) (int64, error) {
	const oper = "client.budget.AddCost"

	var timeNow = timestamppb.Timestamp{
		Seconds: time.Now().Unix(),
		Nanos:   0,
	}

	resp, err := b.clientBudget.AddCost(ctx, &budget.AddCostRequest{
		UserId:    userID,
		Amount:    amount,
		Category:  category,
		CreatedAt: &timeNow,
		Comment:   comment,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return resp.CostId, nil
}
