package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) MonthListCost(ctx context.Context, userID int64, month int32) ([]dto.SumItem, error) {
	const oper = "client.budget.MonthListCost"

	list, err := b.clientBudget.MonthListCost(ctx, &budget.MonthListCostsRequest{
		UserId: userID,
		Month:  month,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	costs := make([]dto.SumItem, len(list.SumCosts))
	for i, cost := range list.SumCosts {
		costs[i] = dto.SumItem{}
		costs[i].Category = cost.Category
		costs[i].Amount = cost.Amount
	}

	return costs, nil
}
