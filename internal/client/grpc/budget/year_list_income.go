package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) YearListIncome(ctx context.Context, userID int64, year int32) ([]dto.SumItem, error) {
	const oper = "client.budget.YearListIncome"

	list, err := b.clientBudget.YearListIncome(ctx, &budget.YearListIncomesRequest{
		UserId: userID,
		Year:   year,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	costs := make([]dto.SumItem, len(list.SumIncomes))
	for i, cost := range list.SumIncomes {
		costs[i] = dto.SumItem{}
		costs[i].Category = cost.Category
		costs[i].Amount = cost.Amount
	}

	return costs, nil
}
