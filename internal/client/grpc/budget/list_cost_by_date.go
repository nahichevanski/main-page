package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) ListCostByDate(ctx context.Context, userID int64, date string) ([]dto.CostView, error) {
	const oper = "client.budget.ListCostByDate"

	resp, err := b.clientBudget.ListCostsByDate(ctx, &budget.ListCostsByDateRequest{
		Date:   date,
		UserId: userID,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	costs := make([]dto.CostView, len(resp.ListCosts))
	for i, cost := range resp.ListCosts {
		costs[i] = dto.CostView{}
		costs[i].ID = cost.Id
		costs[i].Amount = cost.Amount
		costs[i].Category = cost.Category
		costs[i].Comment = cost.Comment
	}

	return costs, nil
}
