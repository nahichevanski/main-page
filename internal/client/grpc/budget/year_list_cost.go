package budget

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *ClientBudget) YearListCost(ctx context.Context, userID int64, year int32) ([]dto.SumItem, error) {
	const oper = "client.budget.YearListCost"

	list, err := b.clientBudget.YearListCost(ctx, &budget.YearListCostsRequest{
		UserId: userID,
		Year:   year,
	})
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	costs := make([]dto.SumItem, len(list.SumCosts))
	for i, cost := range list.SumCosts {
		costs[i] = dto.SumItem{}
		costs[i].Category = cost.Category
		costs[i].Amount = cost.Amount
	}

	return costs, nil
}
