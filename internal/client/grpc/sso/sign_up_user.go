package sso

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"main-page/internal/lib/sl"
)

func (c *ClientAuth) SignUpUser(ctx context.Context, email, password string) (int64, error) {
	const oper = "client.sso.SignUp"

	resp, err := c.clientAuth.SignUp(ctx, &sso.SignUpRequest{
		Email:    email,
		Password: password,
	})
	if err != nil {
		c.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return resp.UserId, nil
}
