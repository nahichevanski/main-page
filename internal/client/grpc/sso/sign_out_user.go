package sso

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"main-page/internal/lib/sl"
)

func (c *ClientAuth) SignOutUser(ctx context.Context, userID int64) (bool, error) {
	const oper = "client.sso.SignOutUser"

	resp, err := c.clientAuth.SignOut(ctx, &sso.SignOutRequest{
		UserId: userID,
	})
	if err != nil {
		c.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return resp.IsOut, nil
}
