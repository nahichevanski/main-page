package sso

import (
	"context"
	"fmt"
	"gitlab.com/nahichevanski/proto-sso/gen/go/sso"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (c *ClientAuth) SignInUser(ctx context.Context, email, password string) (dto.Token, error) {
	const oper = "client.sso.SignInUser"

	resp, err := c.clientAuth.SignIn(ctx, &sso.SignInRequest{
		Email:    email,
		Password: password,
	})
	if err != nil {
		c.log.Error(oper, sl.Err(err))
		return dto.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	token := dto.Token{
		AccessToken:  resp.AccessToken,
		RefreshToken: resp.RefreshToken,
	}

	return token, nil
}
