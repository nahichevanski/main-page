package sso

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"

	"gitlab.com/nahichevanski/proto-sso/gen/go/sso"
)

func (c *ClientAuth) RefreshSession(ctx context.Context, userId int64, token dto.Token) (dto.Token, error) {
	const oper = "client.sso.RefreshSession"

	resp, err := c.clientAuth.Refresh(ctx, &sso.RefreshRequest{
		UserId:       userId,
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	})
	if err != nil {
		c.log.Error(oper, sl.Err(err))
		return dto.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	newToken := dto.Token{
		AccessToken:  resp.AccessToken,
		RefreshToken: resp.RefreshToken,
	}

	return newToken, nil
}
