package budget

import (
	"context"
	"main-page/internal/dto"
)

type ClientBudget interface {
	AddCost(ctx context.Context, userID int64, cost float64, category, comment string) (int64, error)
	AddIncome(ctx context.Context, userID int64, income float64, category, comment string) (int64, error)
	DeleteCost(ctx context.Context, userID, costID int64) (bool, error)
	DeleteIncome(ctx context.Context, userID, incomeID int64) (bool, error)
	ListCostByDate(ctx context.Context, userID int64, date string) ([]dto.CostView, error)
	ListIncomeByDate(ctx context.Context, userID int64, date string) ([]dto.IncomeView, error)
	WeekListCost(ctx context.Context, userID int64) ([]dto.SumItem, error)
	MonthListCost(ctx context.Context, userID int64, month int32) ([]dto.SumItem, error)
	YearListCost(ctx context.Context, userID int64, year int32) ([]dto.SumItem, error)
	MonthListIncome(ctx context.Context, userID int64, month int32) ([]dto.SumItem, error)
	YearListIncome(ctx context.Context, userID int64, year int32) ([]dto.SumItem, error)
}
