package auth

import (
	"context"

	"main-page/internal/dto"
)

type ClientAuth interface {
	SignInUser(ctx context.Context, username, password string) (dto.Token, error)
	SignUpUser(ctx context.Context, username, password string) (int64, error)
	RefreshSession(ctx context.Context, userId int64, token dto.Token) (dto.Token, error)
	SignOutUser(ctx context.Context, userID int64) (bool, error)
	RemoveUser(ctx context.Context, userID int64) (bool, error)
}
