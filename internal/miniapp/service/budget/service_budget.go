package budget

import (
	"context"
	"main-page/internal/dto"
)

type Budget interface {
	AddCostToBudget(ctx context.Context, userId int64, amount float64, category, comment string) (int64, error)
	AddIncomeToBudget(ctx context.Context, userId int64, amount float64, category, comment string) (int64, error)
	DeleteCostFromBudget(ctx context.Context, userID, costId int64) (bool, error)
	DeleteIncomeFromBudget(ctx context.Context, userID, incomeId int64) (bool, error)
	ListCostByDateFromBudget(ctx context.Context, userID int64, date string) ([]dto.CostView, error)
	ListIncomeByDateFromBudget(ctx context.Context, userID int64, date string) ([]dto.IncomeView, error)
	WeekListCostFromBudget(ctx context.Context, userID int64) ([]dto.SumItem, error)
	MonthListCostFromBudget(ctx context.Context, userID int64, month int) ([]dto.SumItem, error)
	YearListCostFromBudget(ctx context.Context, userID int64, year int) ([]dto.SumItem, error)
	MonthListIncomeFromBudget(ctx context.Context, userID int64, month int) ([]dto.SumItem, error)
	YearListIncomeFromBudget(ctx context.Context, userID int64, year int) ([]dto.SumItem, error)
}
