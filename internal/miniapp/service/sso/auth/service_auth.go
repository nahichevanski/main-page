package auth

import (
	"context"

	"main-page/internal/dto"
)

type Auth interface {
	RegisterNewUser(ctx context.Context, username, password string) (int64, error)
	EnterUser(ctx context.Context, email, password string) (dto.Token, error)
	Refresh(ctx context.Context, userID float64, accessToken, refreshToken string) (dto.Token, error)
	OutUser(ctx context.Context, userID int64) (bool, error)
	DeleteUser(ctx context.Context, userID int64) (bool, error)
}
