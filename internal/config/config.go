package config

import (
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type Config struct {
	Server       httpServer   `yaml:"server"`
	SSOClient    clientSSO    `yaml:"sso-client"`
	BudgetClient clientBudget `yaml:"budget-client"`
	Cookie       cookie       `yaml:"cookie"`
}

type httpServer struct {
	Addr        string        `yaml:"addr" env-required:"true"`
	Timeout     time.Duration `yaml:"timeout" env-required:"true"`
	IdleTimeout time.Duration `yaml:"idle_timeout" env-required:"true"`
}

type clientSSO struct {
	Addr         string        `yaml:"addr" env-required:"true"`
	Timeout      time.Duration `yaml:"timeout" env-required:"true"`
	RetriesCount int           `yaml:"retries_count" env-required:"true"`
}

type clientBudget struct {
	Addr         string        `yaml:"addr" env-required:"true"`
	Timeout      time.Duration `yaml:"timeout" env-required:"true"`
	RetriesCount int           `yaml:"retries_count" env-required:"true"`
}

type cookie struct {
	MaxAge int `yaml:"max_age" env-required:"true"`
}

var cfg Config

func MustLoad() *Config {
	err := cleanenv.ReadConfig("./config/local.yaml", &cfg)
	if err != nil {
		panic(err)
	}

	err = godotenv.Load()
	if err != nil {
		panic(err)
	}

	return &cfg
}
