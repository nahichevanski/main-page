package app

import (
	"context"
	"log/slog"
	"main-page/internal/client/grpc/budget"
	"main-page/internal/client/grpc/sso"
	"main-page/internal/config"
	"main-page/internal/server"
	servicebudget "main-page/internal/service/budget"
	serviceauth "main-page/internal/service/sso/auth"
)

type App struct {
	HTTPServer    *server.Server
	SSOGRPCClient *sso.ClientAuth
}

func New(log *slog.Logger, cfg *config.Config) *App {
	ctx := context.Background()
	ssoClient, err := sso.New(ctx, cfg, log)
	if err != nil {
		panic(err)
	}

	budgetClient, err := budget.New(ctx, cfg, log)
	if err != nil {
		panic(err)
	}

	serviceAuth := serviceauth.New(log, ssoClient)
	serviceBudget := servicebudget.New(log, budgetClient)

	return &App{
		HTTPServer:    server.New(cfg, serviceAuth, serviceBudget),
		SSOGRPCClient: ssoClient,
	}
}
