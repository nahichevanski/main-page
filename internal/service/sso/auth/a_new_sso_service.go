package auth

import (
	"log/slog"
	"main-page/internal/miniapp/client/sso/auth"
)

type ServiceAuth struct {
	log    *slog.Logger
	client auth.ClientAuth
}

func New(log *slog.Logger, client auth.ClientAuth) *ServiceAuth {

	return &ServiceAuth{
		log:    log,
		client: client,
	}
}
