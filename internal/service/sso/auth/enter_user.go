package auth

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (s *ServiceAuth) EnterUser(ctx context.Context, email, password string) (dto.Token, error) {
	const oper = "service.sso.EnterUser"

	token, err := s.client.SignInUser(ctx, email, password)
	if err != nil {
		s.log.Error(oper, sl.Err(err))
		return dto.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	return token, nil
}
