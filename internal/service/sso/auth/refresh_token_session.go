package auth

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (s *ServiceAuth) Refresh(ctx context.Context, userID float64, accessToken, refreshToken string) (dto.Token, error) {
	const oper = "service.auth.Refresh"

	uid := int64(userID)

	token := dto.Token{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	token, err := s.client.RefreshSession(ctx, uid, token)
	if err != nil {
		s.log.Error(oper, sl.Err(err))
		return dto.Token{}, fmt.Errorf("%s: %w", oper, err)
	}

	return token, nil
}
