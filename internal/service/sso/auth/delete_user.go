package auth

import (
	"context"
	"fmt"
	"main-page/internal/lib/sl"
)

func (s *ServiceAuth) DeleteUser(ctx context.Context, userID int64) (bool, error) {
	const oper = "service.sso.DeleteUser"

	ok, err := s.client.RemoveUser(ctx, userID)
	if err != nil {
		s.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return ok, nil
}
