package auth

import (
	"context"
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"main-page/internal/lib/sl"
	"main-page/internal/service"
)

func (s *ServiceAuth) RegisterNewUser(ctx context.Context, email, password string) (int64, error) {
	const oper = "service.sso.RegisterNewUser"

	userId, err := s.client.SignUpUser(ctx, email, password)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			return 0, service.ErrUserAlreadyExists
		}
		s.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return userId, nil
}
