package auth

import (
	"context"
	"fmt"
	"main-page/internal/lib/sl"
)

func (s *ServiceAuth) OutUser(ctx context.Context, userID int64) (bool, error) {
	const oper = "service.sso.OutUser"

	ok, err := s.client.SignOutUser(ctx, userID)
	if err != nil {
		s.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return ok, nil
}
