package budget

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *Budget) YearListCostFromBudget(ctx context.Context, userID int64, year int) ([]dto.SumItem, error) {
	const oper = "service.budget.YearListCostFromBudget"

	list, err := b.clientBudget.YearListCost(ctx, userID, int32(year))
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return list, nil
}
