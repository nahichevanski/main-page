package budget

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *Budget) MonthListCostFromBudget(ctx context.Context, userID int64, month int) ([]dto.SumItem, error) {
	const oper = "service.budget.MonthListCostFromBudget"

	list, err := b.clientBudget.MonthListCost(ctx, userID, int32(month))
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return list, nil
}
