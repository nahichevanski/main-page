package budget

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *Budget) ListIncomeByDateFromBudget(ctx context.Context, userID int64, date string) ([]dto.IncomeView, error) {
	const oper = "service.budget.ListIncomeByDateFromBudget"

	list, err := b.clientBudget.ListIncomeByDate(ctx, userID, date)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return list, nil
}
