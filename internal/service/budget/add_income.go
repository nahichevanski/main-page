package budget

import (
	"context"
	"fmt"
	"main-page/internal/lib/sl"
)

func (b *Budget) AddIncomeToBudget(ctx context.Context, userId int64, amount float64, category, comment string) (int64, error) {
	const oper = "service.budget.AddIncomeToBudget"

	incomeID, err := b.clientBudget.AddIncome(ctx, userId, amount, category, comment)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return incomeID, nil
}
