package budget

import (
	"log/slog"

	clientbudget "main-page/internal/miniapp/client/budget"
)

type Budget struct {
	log          *slog.Logger
	clientBudget clientbudget.ClientBudget
}

func New(log *slog.Logger, clientBudget clientbudget.ClientBudget) *Budget {
	return &Budget{
		log:          log,
		clientBudget: clientBudget,
	}
}
