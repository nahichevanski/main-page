package budget

import (
	"context"
	"fmt"
	"main-page/internal/lib/sl"
)

func (b *Budget) AddCostToBudget(ctx context.Context, userId int64, amount float64, category, comment string) (int64, error) {
	const oper = "service.budget.AddCostToBudget"

	costID, err := b.clientBudget.AddCost(ctx, userId, amount, category, comment)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return costID, nil
}
