package budget

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *Budget) WeekListCostFromBudget(ctx context.Context, userID int64) ([]dto.SumItem, error) {
	const oper = "service.budget.WeekListCostFromBudget"

	list, err := b.clientBudget.WeekListCost(ctx, userID)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return list, nil
}
