package budget

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *Budget) ListCostByDateFromBudget(ctx context.Context, userID int64, date string) ([]dto.CostView, error) {
	const oper = "service.budget.ListCostByDateFromBudget"

	list, err := b.clientBudget.ListCostByDate(ctx, userID, date)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return list, nil
}
