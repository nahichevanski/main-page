package budget

import (
	"context"
	"fmt"
	"main-page/internal/dto"
	"main-page/internal/lib/sl"
)

func (b *Budget) YearListIncomeFromBudget(ctx context.Context, userID int64, year int) ([]dto.SumItem, error) {
	const oper = "service.budget.YearListIncomeFromBudget"

	list, err := b.clientBudget.YearListIncome(ctx, userID, int32(year))
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return list, nil
}
