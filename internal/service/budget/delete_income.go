package budget

import (
	"context"
	"fmt"
	"main-page/internal/lib/sl"
)

func (b *Budget) DeleteIncomeFromBudget(ctx context.Context, userID, incomeID int64) (bool, error) {
	const oper = "service.budget.DeleteIncomeFromBudget"

	ok, err := b.clientBudget.DeleteIncome(ctx, userID, incomeID)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return ok, nil
}
