package budget

import (
	"context"
	"fmt"
	"main-page/internal/lib/sl"
)

func (b *Budget) DeleteCostFromBudget(ctx context.Context, userID, costID int64) (bool, error) {
	const oper = "service.budget.DeleteCostFromBudget"

	ok, err := b.clientBudget.DeleteCost(ctx, userID, costID)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return ok, nil
}
