package dto

type CostView struct {
	ID       int64   `json:"id"`
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
	Comment  string  `json:"comment"`
}

type IncomeView struct {
	ID       int64   `json:"id"`
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
	Comment  string  `json:"comment"`
}

type SumItem struct {
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
}
