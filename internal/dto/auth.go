package dto

type Token struct {
	AccessToken  string
	RefreshToken string
}
