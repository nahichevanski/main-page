package main

import (
	"log/slog"
	"main-page/internal/app"
	"main-page/internal/config"
	"main-page/internal/lib/slogpretty"
	"os"
)

const (
	envLocal = "local"
	envProd  = "prod"
)

func main() {
	//ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	//defer stop()

	cfg := config.MustLoad()

	log := setupLogger(envLocal)

	application := app.New(log, cfg)

	log.Info("Application started", slog.String("address", cfg.Server.Addr))
	err := application.HTTPServer.Run()
	if err != nil {
		panic(err)
	}

	//<-ctx.Done()
	//stop()
	//log.Info("Application stopped")
	//
	//ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	//defer cancel()
	//
	//if err := application.HTTPServer.Stop(ctx); err != nil {
	//	log.Error("Server forced to shutdown: ", err)
	//	panic(err)
	//}
	//
	////all must be stopped
	//
	//log.Error("Server exiting")

}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case envLocal:
		log = setupPrettySlog()
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return log
}

func setupPrettySlog() *slog.Logger {
	opts := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}

	handler := opts.NewPrettyHandler(os.Stdout)

	return slog.New(handler)
}
